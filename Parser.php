<?php

include 'iParser.php';

header('Content-type: application/json; charset=UTF-8') ;
ini_set('display_errors','1');
error_reporting(E_ALL & ~E_NOTICE);

$PROXY = array(
    'http' => array(
        'proxy' => 'proxy-srv.csi.it:3128',
        'request_fulluri' => true,
    ),
);

class Parser implements iParser{


    function __construct() {}

    public function rssToJson($url){

        if($_SERVER['HTTP_HOST'] == 'messages.portali.csi.it'){
            global $PROXY;
            $ctx = stream_context_create($PROXY);
            $rss = file_get_contents($url, False, $ctx);
        } else{ 
            $rss = file_get_contents($url);
        }
        $rss = str_replace(array("\n", "\r", "\t"), '', $rss);
        $rss = trim(str_replace('"', "'", $rss));
        $xml = simplexml_load_string($rss);
        $json = json_encode($xml, JSON_UNESCAPED_UNICODE);
        $json = trim(str_replace('\/\/', "//", $json));
        $json = trim(str_replace('\/', "/", $json));
        return $json;
    }

    public function getTitle($json){
        $json = json_decode($json, true);
        return $json["channel"]["title"];
    }

    public function getDescription($json){
        $json = json_decode($json, true);
        return $json["channel"]["description"];
    }

    public function getVoti($json){
        $res = array();
        $json = json_decode($json, true);
        $json = $json['channel']['item'];
        foreach ($json as $value) {
            $description = str_replace ("n. voti:","numero voti", $value['description']);
            $description = str_replace (" - "," corrispondente al ", $description);
            $res[] = $value['title'] . " - " . $description;  
        }
        $res = array_merge($res);
        return json_encode(array('items'=>$res));
    } 

    public function getAllInfo($json){
        $json = json_decode($json, true);
        $json2 = $json['channel']['item'];
        $json = $json['channel'];
        $response = "";
        foreach ($json2 as $value) {
            $description = str_replace ("n. voti:","", $value['description']);
            $description = str_replace (" - "," voti, corrispondente al ", $description);
            $response .= $value['title'] . " ha ottenuto " . $description . ". ";
        }
        $response .= $json['description'] . ". ";
        return json_encode(array('response'=>$response));
    } 

    public function getCandidato($json,$id){
        $json = json_decode($json, true);
        $json2 = $json['channel']['item'];
        $json = $json['channel'];
        $response = "Non ho trovato candidati con questo nome, riprova.";
        foreach ($json2 as $value) {
            $id = str_replace("_"," ", $id);
            if(strcasecmp($id, $value['title']) == 0){
                $response  = " ";
                $description = str_replace ("n. voti:","", $value['description']);
                $description = str_replace (" - "," voti, corrispondente al ", $description);
                $response .= $value['title'] . " ha ottenuto " . $description . ". ";
                $response .= $json['description'] . ". ";
                break;
            }
        }
        return json_encode(array('response'=>$response));
    } 

    public function getCandidati($json){
        $json = json_decode($json, true);
        $json = $json['channel']['item'];
        $response = null;
        foreach ($json as $value) {
            $response .= $value['title'] . ", ";
            }
        return json_encode(array('response'=>substr($response, 0, -2)));
    }
    
    public function getNCandidati($json,$limit){
        $json = json_decode($json, true);
        $json2 = $json['channel']['item'];
        $json = $json['channel'];
        if($limit > count($json2)) return json_encode(array('response'=>'Ci sono solo ' . count($json2) . ' candidati o liste, riprovare!'));
        $response = "";
        for ($i=0; $i < count($json2); $i++) {
            $description = str_replace ("n. voti:","", $json2[$i]['description']);
            $description = str_replace (" - "," voti, corrispondente al ", $description);
            $response .= $json2[$i]['title'] . " ha ottenuto " . $description . ". ";
            if($limit == $i+1) break;
        }
        $response .= $json['description'] . ". ";
        return json_encode(array('response'=>$response));
    } 

}

?>