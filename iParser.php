<?php

/**
 * Interfaccia per il parsificatore Parser che parsifica ed estrae informazioni da una fonte RSS
 */
interface iParser{

    /**
     * Converte una fonte RSS in formato JSON
     * @param string $url link della fonte RSS
     */
    public function rssToJson($url);
    
    /**
     * Ritorna il titolo del JSON
     * @param string $json json da processare
     */
    public function getTitle($json);
    
    /**
     * Ritorna la descrizione del JSON
     * @param string $json json da processare
     */
    public function getDescription($json);
    
    /**
     * Ritorna un JSON con la lista dei candidati con i relativi voti
     * @param string $json json da processare
     */
    public function getVoti($json);

    /**
     * Ritorna un JSON con una unica stringa con tutte le informazioni del json, i candidati ed i loro voti
     * @param string $json json da processare 
     */
    public function getAllInfo($json); # diventerà getVotiCandidati e getVotiListe

    /**
     * Ritorna un JSON con il tipo di json, il candidato selezionato e i suoi voti
     * @param string $json json da processare
     * @param string $id cognome_nome candidato
     */
    public function getCandidato($json,$id);

    /**
     * Ritorna un JSON con una lista in formato stringa dei candidati o liste
     * @param string $json json da processare
     */
    public function getCandidati($json);


    /**
     * Ritorno un JSON con una lista in formato stringa di N candidati o liste
     */
    public function getNCandidati($json, $limit);

}

?>