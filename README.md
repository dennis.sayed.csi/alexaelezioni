
# Paser RSS to JSON
Script che converte una fonte RSS in formato JSON.

## Richiamo script

- **json**: JSON completo di tutte le informazioni della fonte RSS
    ```
    messages.portali.csi.it/cotoelezioni/extractInfo.php?rss=http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss&action=json
    ```

- **getVoti**: Lista di candidati associati ai voti e percentuali
    ```
    messages.portali.csi.it/cotoelezioni/extractInfo.php?rss=http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss&action=getVoti
    ```

- **title**: Campo title del JSON
    ```
    messages.portali.csi.it/cotoelezioni/extractInfo.php?rss=http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss&action=title
    ```

- **description**: Campo description del JSON
    ```
    messages.portali.csi.it/cotoelezioni/extractInfo.php?rss=http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss&action=description
    ```

- **getCandidati**: tutti i candidati in una stringa formato json
    ```
    messages.portali.csi.it/cotoelezioni/extractInfo.php?rss=http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss&action=getCandidati
    ```

- **getCandidato**: il candidato selezionato e i suoi voti in formato json
    ```
    messages.portali.csi.it/cotoelezioni/extractInfo.php?rss=http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss&action=getCandidato&id=<cognome_nome>
    ```

- **getNCandidati**: gli N candidati selezionati e i suoi voti in formato json
    ```
    messages.portali.csi.it/cotoelezioni/extractInfo.php?rss=http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss&action=getNCandidati&limit=<int>
    ```

## URL elezioni regionali 2014 di esempio
**[Elezioni regionali](http://www.comune.torino.it/elezioni/2014/regionali/rss.shtml)**
- **[Elezione del Presidente della Regione](http://www.comune.torino.it/elezioni/2014/regionali/rss/presidente/citta/risultati.rss)**
- **[Elezione del Consiglio Regionale](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/risultati.rss)**
- **Elezione del Consiglio Regionale: preferenze**
    - Liste collegate a FILINGERI MAURO
        - [L'ALTRO PIEMONTE A SINISTRA](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-1/preferenze.rss)
    
    - Liste collegate a COSTA ENRICO
        - [NUOVO CENTRO DESTRA - UDC](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-2/preferenze.rss)
    - Liste collegate a CROSETTO GUIDO
        - [FRATELLI D'ITALIA - ALLEANZA NAZIONALE](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-3/preferenze.rss)
    - Liste collegate a CHIAMPARINO SERGIO
        - [SINISTRA ECOLOGIA LIBERTÀ](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-5/preferenze.rss)
        - [MODERATI](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-6/preferenze.rss)
        - [SCELTA CIVICA](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-6/preferenze.rss)
        - [CHIAMPARINO PER IL PIEMONTE](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-7/preferenze.rss)
        - [PARTITO DEMOCRATICO](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-8/preferenze.rss)
        - [ITALIA DEI VALORI](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-9/preferenze.rss)
    - Liste collegate a BONO DAVIDE
        - [MOVIMENTO 5 STELLE BEPPEGRILLO.IT](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-10/preferenze.rss)
    - Liste collegate a PICHETTO FRATIN GILBERTO
        - [VERDI - VERDI](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-11/preferenze.rss)
        - [PARTITO PENSIONATI](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-12/preferenze.rss)
        - [GRANDE SUD - AZZURRI ITALIANI](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-13/preferenze.rss)
        - [DESTRE UNITE](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-14/preferenze.rss)
        - [LEGA NORD - BASTA €URO](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-15/preferenze.rss)
        - [CIVICA PER IL PIEMONTE](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-16/preferenze.rss)
        - [FORZA ITALIA](http://www.comune.torino.it/elezioni/2014/regionali/rss/consreg/citta/lista-17/preferenze.rss)

## URL elezioni europee 2014 di esempio
**[Elezioni europee](http://www.comune.torino.it/elezioni/2014/europee/rss.shtml)**
- [**Elezione del Parlamento Europeo: voti di lista**](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/risultati.rss)
- **Elezione del Parlamento Europeo: preferenze**
    -   [VERDI EUROPEI - GREEN ITALIA](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-1/preferenze.rss)
    -   [ITALIA DEI VALORI](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-2/preferenze.rss)
    -   [L'ALTRA EUROPA CON TSIPRAS](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-3/preferenze.rss)
    -   [MOVIMENTO 5 STELLE BEPPEGRILLO.IT](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-4/preferenze.rss)
    -   [FRATELLI D'ITALIA - ALLEANZA NAZIONALE](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-5/preferenze.rss)
    -   [SCELTA EUROPEA](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-6/preferenze.rss)
    -   [IO CAMBIO - MAIE](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-7/preferenze.rss)
    -   [FORZA ITALIA](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-8/preferenze.rss)
    -   [NUOVO CENTRO DESTRA - UDC](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-9/preferenze.rss)
    -   [PARTITO DEMOCRATICO](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-10/preferenze.rss)
    -   [LEGA NORD - DIE FREIHEITLICHEN - BASTA €URO](http://www.comune.torino.it/elezioni/2014/europee/rss/parlamento/citta/lista-11/preferenze.rss)