<?php

include 'Parser.php';

$parser = new Parser();

switch($_REQUEST['action']){

    #JSON integrale della fonte RSS
    case 'json':
        $json = $parser->rssToJson($_REQUEST['rss']);
        echo $json;
        return $json;

    #Campo title della fonte RSS
    case 'title':
        $json = $parser->rssToJson($_REQUEST['rss']);
        $out = $parser->getTitle($json);
        echo $out;
        return $out;

    #Campo description della fonte RSS
    case 'description':
        $json = $parser->rssToJson($_REQUEST['rss']);
        $out = $parser->getDescription($json);
        echo $out;
        return $out;

    
    case 'getVoti':
        $json = $parser->rssToJson($_REQUEST['rss']);
        $out = $parser->getVoti($json);
        echo $out;
        return $out;

    case 'getAllInfo':
        $json = $parser->rssToJson($_REQUEST['rss']);
        $out = $parser->getAllInfo($json);
        echo $out;
        return $out;

    case 'getCandidato':
        $json = $parser->rssToJson($_REQUEST['rss']);
        $out = $parser->getCandidato($json,$_REQUEST['id']);
        echo $out;
        return $out; 
        
    case 'getCandidati':
        $json = $parser->rssToJson($_REQUEST['rss']);
        $out = $parser->getCandidati($json);
        echo $out;
        return $out; 

    case 'getNcandidati':
        $json = $parser->rssToJson($_REQUEST['rss']);
        $out = $parser->getNCandidati($json,$_REQUEST['limit']);
        echo $out;
        return $out;
}

?>